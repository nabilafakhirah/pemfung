import Data.Maybe

-- No. 2

-- Change to higher order function

-- [ x+1 | x <- xs ]
hofa xs = map (+1) xs

-- [ x+y | x <- xs, y <- ys ]
hofb xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)

-- [ x+2 | x <- xs, y <- ys ]
hofc xs = map (+2) (filter (>3) xs)

-- [ x+3 | (x,_) <- xys ]
hofd xys = map (\(x,_) -> x+3) xys

-- [ x+4 | (x, y) <- xys, x+y < 5 ]
hofe xys = map (\(x,_) -> x+ 4) (filter (\(x, y) -> x+y <5) xys)

-- [ x+5 | Just x <- mxs ]
hoff mxs = map (\(Just x) -> x+5) (filter isJust mxs)

-- Change to List Comprehension

-- map (+3) xs
lc1 xs = [ x+3 | x <- xs :: [Int]]

-- filter (>7) xs
lc2 xs = [ x | x <- xs :: [Int], x > 7 ]

-- concat (map (\x -> map (\y -> (x, y)) ys) xs)
lc3 xs ys = [ (x,y) | x <- xs, y <- ys ]

-- filter (>3) (map (\(x,y) -> x+y) xys)
lc4 xys = [ x+y | (x,y) <- xys, x+y > 3]
module Robot where

import Data.Array
import Data.List
import Control.Monad
import Control.Applicative
import SOE
import Win32Misc (timeGetTime)
import qualified GraphicsWindows as GW (getEvent)

data Direction = North 
                | East 
                | South 
                | West 
                deriving (Eq,Show,Enum)

right :: Direction -> Direction
right d = toEnum (succ (fromEnum d) `mod` 4)

data RobotState = RobotState    { position :: Position , facing :: Direction
                                , pen :: Bool , color :: Color
                                , treasure :: [Position], pocket :: Int
                                } deriving Show

type Position = (Int,Int)

updateState u = Robot (\s _ _ -> return (u s, ()))

colors :: Array Int Color
newtype Robot a = Robot (RobotState -> Grid -> Window -> IO (RobotState, a))

turnRight :: Robot ()
turnRight = updateState (\s -> s {facing = right (facing s)})

moven :: Int -> Robot ()
moven n = mapM_ (const move) [1..n]

-- Nomor 1 Higher Order Function.

--a
length p = sum (map (\x -> 1) l)

--b
{- 
map (+1) menambahkan masing-masing elemen pada list dengan bilangan 1. misal ada list [1, 2, 3], output yang
keluar adalah [2, 3, 4]. berhubung map (+1) dijalankan dua kali, maka penjumlahan dilakukan dua kali. sehingga
misal input list [1, 2, 3], maka output yang diberikan adalah [3, 4, 5]
-}

--c
iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter (n-1) f x)

--d
-- \n -> iter n succ merupakan fungsi tanpa nama yang mengambil 2 parameter, n dan angka yang kit inginkan untuk succ-nya.

--e
sumSquares n = foldr (+) 0 $ map (\x -> x*x) [1..n]

--f
{- 
fungsi misteri ini memasukkan elemen yang ada pada xs ke dalam sebuah list, kemudian di-concatenate dengan list kosong.
output yang diberikan merupakan input list sendiri.
-}

--g
{-
(id . f) sama dengan f. hasil dari f yang berupa boolean digunakan identity function, return value boolean.
(id :: Bool -> Bool)

(f . id) sama dengan f, argumen f integer digunakan identity function dan return value integer
(id :: Int -> Int)

id f sama dengan f. identity function tidak merubah output dari f. (id :: (Int -> Bool) -> (Int -> Bool))
-}

--h
composeList :: [a -> a] -> (a -> a)
composeList []     = id
composeList (f:fs) = f . composeList fs

-- i
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = \x y -> f y x

{-
fungsi ini menerima fungsi f dan dua parameter. jika dijalankan, flipping akan menukar kedua parameter
posisi dari kedua parameter tersebut
-}

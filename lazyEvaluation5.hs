primes = sieve [2 .. 10]
  where sieve (x:xs) = x : (sieve [z | z <- xs, z `mod` x /= 0])
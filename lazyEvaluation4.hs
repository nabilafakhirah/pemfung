import Data.List

perm [] = [[]]
perm ls = [ x:ps | x <- ls, ps <- perm(ls\\[x])]
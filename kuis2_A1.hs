notNumber x = x `notElem` "1234567890"
numberRemoval = filter notNumber

isVowel x = x `elem` "aiueoAIUEO"
countVowel = length . filter isVowel
isThreeOrLessVowels str = countVowel str < 4

doesNotAppearTwiceInARow (a:b:ls) | (a==b) = False
doesNotAppearTwiceInARow (a:ls) = doesNotAppearTwiceInARow ls
doesNotAppearTwiceInARow [] = True

weirdFilter = filter isThreeOrLessVowels . filter doesNotAppearTwiceInARow . map numberRemoval

{- 
Write a function rotabc that changes a's to b's, b's to c's, and c's to
a's in a string. Only lowercase letters are affected.
-}

rotabc (x:xs) | x == 'a' = 'b' : rotabc xs
              | x == 'b' = 'c' : rotabc xs
              | x == 'c' = 'a' : rotabc xs
              | _ = x : rotabc xs
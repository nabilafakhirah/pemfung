import Data.List 
import qualified Data.Char as Char

notNumber x = x `notElem` "1234567890"
numberRemoval = filter notNumber

isVowel x = x `elem` "aiueoAIUEO"
countVowel = length . filter isVowel
isThreeOrLessVowels str = countVowel str < 4

doesNotAppearTwiceInARow (a:b:ls) | (a==b) = False
doesNotAppearTwiceInARow (a:ls) = doesNotAppearTwiceInARow ls
doesNotAppearTwiceInARow [] = True

weirdFilter = filter isThreeOrLessVowels . filter doesNotAppearTwiceInARow . map numberRemoval

{- 
Write a function rotabc that changes a's to b's, b's to c's, and c's to
a's in a string. Only lowercase letters are affected.
-}

rotabc1 (x:xs) | x == 'a' = 'b' : rotabc xs
              | x == 'b' = 'c' : rotabc xs
              | x == 'c' = 'a' : rotabc xs
              | otherwise = x : rotabc xs

rotabc1 [] = []

-- only works if u add the thingamabob at the end
            

rotabc = map abc
    where abc 'a' = 'b'
          abc 'b' = 'c'
          abc 'c' = 'a'
          abc x = x

{-
last function while applying point-free style. last returns last
element of a list
-}

last' = head . reverse

{-
returns the same sentence with all words capitalized except the
ones from the list given
-}

getWord word [] = (word, [])
getWord word(x:xs) | x == ' ' = (word, xs)
                   | x /= ' ' = getWord (word ++ [x]) xs 

splitter lw []  = lw
splitter lw inp = let (word, rest) = getWord "" inp
                  in splitter (lw ++ [word]) rest 

upper el word = if word `elem` el
                then word
                else Char.toUpper (head word) : (tail word)

upper2 el word@(h:t) = if word `elem` el
                       then word
                       else Char.toUpper h : t 

combine [] = []
combine [a] = a
combine (a:xs) = a ++ " " ++ (combine xs)

capitalize inp el = let lw = splitter [] inp
                    in combine (map (upper el) lw)

{-
Definition of composition function (compose) similar to the compose/dot
operator in Haskell prelude, that accept two functions and return a new
function. Define the type of the compose function
-}

compose :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
compose a b = (\x -> a(b x))

{-
Definisikan fungsi last, dengan menerapkan foldr atau foldl. Fungsi
last tersebut menerima sebuah list dan mengembalikan elemen terakhir
dari list tersebut.
-}

last2 :: [a] -> a
last2 = foldl (\_ x -> x) undefined
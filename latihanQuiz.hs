-- LATIHAN QUIZ

-- No. 1 Kelas A
maxTiga :: Ord a => a -> a -> a -> a
maxTiga a b c = max a (max b c)

-- No. 2 Kelas A
-- [ (x, y) | x <- [1 .. 4], y <- [2 .. 6], x * 2 == y ]
{-
Pertama-tama, ekspresi di atas akan membuat list x dan y, dimana x adalah bilangan dari
1 hingga 4, dan y bilangan 2 hingga 6. Setelah itu, seluruh elemen dari x akan dikalian
dengan 2. Hasil perkalian yang memiliki pasangan yang sama dengan y akan dimasukkan ke
dalam list output dengan pasangannya. Hasilnya adalah (1,2), (2,4), (3,6)
-}

-- No. 1 Kelas B
kpk :: Integral a => a -> a -> a
kpk a b = lcm a b

findKpk a b = head [ x | x <- [1..(a*b)], x `mod` a == 0, x `mod` b == 0]

-- No. 3 Kelas A
quickSort :: Ord a => [a] -> [a]
quickSort [] = []
quickSort (x:xs) = quickSort[y | y <- xs, y <= x] ++ quickSort[y | y <- xs, y > x]

-- No. 4 Kelas A
jumlahList [] = 0
jumlahList xs = foldl (+) 0 xs

-- No. 5 Kelas A
{-
misteri [1,2,3] [4,5,6]
concat (map (\x -> map (\y -> (x, y)) ys) xs)
concat (map \x -> (x, 4), (x,5), (x,6))
concat ((1, 4), (2, 5), (3, 6))
[(1,4), (2,5), (3,6)]
-}

-- No. 7 Kelas A
--flip :: (a -> b -> c) -> b -> a -> c

-- No. 2 Kelas B
-- [ (x, y) | x <- (1 .. 3), y <- (1 .. x*2)]
{-
Ekspresi ini akan menyatukan x dan y. Elemen dari x adalah 1 2 dan 3, dan y adalah
1 hingga 2 * x. Untuk setiap x akan dipasangkan dengan 1 hingga 2*x. Output dari
ekspresi tersebut adalah [(1,1), (1,2), (2,1), (2,2), (2,3), (2,4), (3,1), (3,2)
(3,3), (3,4), (3,5), (3,6)]
-}

-- No. 4 Kelas B
maxList xs = foldl max 0 xs

-- No. 6 Kelas B
pythagoras = [ (x,y,z) | z <- [1 ..], y <- [1 .. z-1], x <- [1 .. y-1], x**2 + y**2 == z**2]

--No. 10
{-
toEnum merupakan fungsi yang mengubah integer menjadi suatu tipe data tertentu. misal, jika kita menggunakan
toEnum 35, maka output akan berupa '#'. dalam kasus ini, toEnum mengubah integer menjadi direction. hal ini
dapat terjadi karena kelas direction derived enum.
-}